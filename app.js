// constants required from other files.
const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const app = express();
const reserves = require('./Reserves');


// Init middleware 

//Handlebars Middleware
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Body parser Middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.get('/contact', (req, res) => res.render('contact', { title: 'Reserve a table.', reserves}));

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Booking API route
app.use('/api/reserves', require('./routes/api/reserves'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
