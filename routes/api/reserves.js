const express = require('express');
const router = express.Router();
const reserves = require('../../Reserves')

// Gets reserves from list.
router.get('/', (req, res) => res.json(reserves));

// Get single booking from API
router.get('/:id', (req, res) => {
    const exist = reserves.some(booking => booking.id === parseInt(req.params.id));

    if (exist) {
        res.json(reserves.filter(booking => booking.id === parseInt(req.params.id)));
    } else {
        res.status(400).json({ msg: `No booking with the id of ${req.params.id} found.` });
    }
});

// Create reserve
router.post('/', (req, res) => {
    let newId = reserves.length;
    const newBooking = {
        id: parseInt(newId + 1),
        name: req.body.name,
        email: req.body.email,
        message: req.body.message
    }

    if (!newBooking.name || !newBooking.email || !newBooking.message) {
        return res.status(400).json({ msg: "Please include a name a email and a message." });
    }

    reserves.push(newBooking);
    //res.json(reserves);
    res.redirect('/contact');
});

// Update booking
router.put('/:id', (req, res) => {
    const exist = reserves.some(booking => booking.id === parseInt(req.params.id));

    if (exist) {
        const update = req.body;
        reserves.forEach(booking => {
            if (booking.id === parseInt(req.params.id)) {
                booking.name = update.name ? update.name : booking.name;
                booking.email = update.email ? update.email : booking.email;
                booking.message = update.message ? update.message : booking.message;

                res.json({ msg: 'Booking updated', booking })
            }
        });
    } else {
        res.status(400).json({ msg: `No booking with the id of ${req.params.id} found.` });
    }
});

// Delete booking
router.delete('/:id', (req, res) => {
    const exist = reserves.some(booking => booking.id === parseInt(req.params.id));

    if (exist) {
        res.json({ msg: 'Booking deleted', reserves: reserves.filter(booking => booking.id !== parseInt(req.params.id)) });
    } else {
        res.status(400).json({ msg: `No booking with the id of ${req.params.id} found.` });
    }
});

module.exports = router;