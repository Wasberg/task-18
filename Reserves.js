const reserves = [
    {
        id: 1,
        name: 'Bo Karlsson',
        email: 'bo@gmail.com',
        message: 'I want to book a table for two please tomorrow.'
    },
    {
        id: 2,
        name: 'Kalle Åkare',
        email: 'kalle.akare@gmail.com',
        message: 'I want to book the whole resturant on the 30th of february.'
    },
    {
        id: 3,
        name: 'Gonas Jardell',
        email: 'jardell.g@gmail.com',
        message: 'I want to book a table for myself. Today!.'
    }
]

module.exports = reserves;